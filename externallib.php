<?php
/**
 * @package   mod_interactivid
 * @copyright 2017 InteractiVid {@link http://interactivid.com}
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . "/externallib.php");

class mod_interactivid_external extends external_api
{
    /**
     * @return external_single_structure
     */
    private static function get_videos_for_account_structure($required = true) {
        return new external_single_structure(
            array(
                'id' => new external_value(PARAM_INT, 'id'),
                'publicid' => new external_value(PARAM_TEXT, 'public id'),
                'accountid' => new external_value(PARAM_INT, 'account id'),
                'userid' => new external_value(PARAM_INT, 'user id'),
                'title' => new external_value(PARAM_TEXT, 'video title'),
                'hostedby' => new external_value(PARAM_TEXT, 'hosted provider'),
                'duration' => new external_value(PARAM_INT, 'duration in seconds'),
                'durationtime' => new external_value(PARAM_TEXT, 'duration as a time string'),
                'timemodified' => new external_value(PARAM_INT, 'last modified time'),
            ), 'videos', $required
        );
    }

    /**
     * @return external_function_parameters
     */
    public static function get_videos_for_account_parameters() {
		return new external_function_parameters(
			array(
				'courseid' => new external_value(PARAM_INT, 'Course id.'),
				'accountid' => new external_value(PARAM_INT, 'InteractiVid account id.'),
			)
        );
    }

	/**
	 * Returns a list of videos for a specific InteractiVid account.
	 *
	 * @param int $accountid
	 * @return array
	 */
	public static function get_videos_for_account($courseid, $accountid)
	{
		global $CFG;

		$params = self::validate_parameters(self::get_videos_for_account_parameters(),
			['courseid' => $courseid, 'accountid' => $accountid]);

		$context = context_course::instance($courseid);
		self::validate_context($context);

		require_once($CFG->dirroot . '/mod/interactivid/lib.php');
		$videos = interactivid_refresh_videos($accountid, $courseid);
		$videos = interactivid_get_videos($accountid);

		return [
			'videos' => $videos,
			'warnings' => [],
		];
	}

    /**
     * @return external_single_structure
     */
    public static function get_videos_for_account_returns() {
        return new external_single_structure(
            array(
            	'videos'	=> new external_multiple_structure(self::get_videos_for_account_structure(), 'list of InteractiVid videos'),
                'warnings'  => new external_warnings('Item must be an InteractiVid video.')
            )
        );
    }

    /**
     * @return external_function_parameters
     */
    public static function add_api_key_parameters() {
		return new external_function_parameters(
			array(
				'courseid' => new external_value(PARAM_INT, 'Course id.'),
				'apikey' => new external_value(PARAM_TEXT, 'InteractiVid API key.')
			)
		);
    }

	/**
	 * Returns a list of videos for a specific InteractiVid account.
	 *
	 * @param int $courseid
	 * @param int $accountid
	 * @return array
	 */
	public static function add_api_key($courseid, $apikey)
	{
		global $CFG, $DB, $USER;
		require_once($CFG->dirroot . '/mod/interactivid/lib.php');
		require_once($CFG->dirroot . '/mod/interactivid/locallib.php');

		$valid = false;
		$added = false;

		$params = self::validate_parameters(self::add_api_key_parameters(),
			['courseid' => $courseid, 'apikey' => $apikey]);

		$context = context_course::instance($courseid);
		self::validate_context($context);

		$interactivid_user = interactivid_get_user($apikey);
		if ($interactivid_user)
		{
			$account = new stdClass;
			$account->courseid = $courseid;
			$account->userid = $USER->id;
			$account->email = $interactivid_user->email;
			$account->brand = isset($interactivid_user->brand) ? $interactivid_user->brand : 'interactivid';
			$account->apikey = $apikey;
			$account->timemodified = time();

			if (!$DB->record_exists('interactivid_accounts', ['courseid' => $account->courseid, 'userid' => $account->userid, 'apikey' => $account->apikey]))
			{
				if (!$DB->insert_record('interactivid_accounts', $account))
				{
					// TODO: Error-handle.
				}
				else
					return [
						'apikey' => $account->apikey,
						'table_html' => interactivid_generate_accounts_html(interactivid_get_accounts($courseid)),
						'valid' => true,
						'added' => true,
						'warnings' => [],
					];
			}
			else
			{
				// TODO: Display message that API key is already present.
				return [
					'apikey' => $account->apikey,
					'table_html' => '',
					'valid' => true,
					'added' => false,
					'warnings' => ['The API key is already in your list.'],
				];
			}
		}

		return [
			'apikey' => $apikey,
			'table_html' => '',
			'valid' => true,
			'added' => false,
			'warnings' => ['The API key is not invalid.'],
		];
	}

    /**
     * @return external_single_structure
     */
    public static function add_api_key_returns() {
        return new external_single_structure(
            array(
            	'apikey' => new external_value(PARAM_RAW, 'The API key.'),
            	'table_html' => new external_value(PARAM_RAW, 'The table HTML.'),
            	'valid' => new external_value(PARAM_BOOL, 'Whether the new API key is valid.'),
            	'added' => new external_value(PARAM_BOOL, 'Whether the new API key was added.'),
                'warnings' => new external_warnings(),
            )
        );
    }

    /**
     * @return external_function_parameters
     */
    public static function delete_api_key_parameters() {
		return new external_function_parameters(
			array(
				'courseid' => new external_value(PARAM_INT, 'Course id.'),
				'apikey' => new external_value(PARAM_RAW, 'InteractiVid API key.')
			)
		);
    }

	/**
	 * Returns a list of videos for a specific InteractiVid account.
	 *
	 * @param int $courseid
 	 * @param int $accountid
	 * @return array
	 */
	public static function delete_api_key($courseid, $apikey)
	{
		global $CFG, $DB;
		require_once($CFG->dirroot . '/mod/interactivid/lib.php');
		require_once($CFG->dirroot . '/mod/interactivid/locallib.php');

		$valid = false;
		$deleted = false;

		$params = self::validate_parameters(self::delete_api_key_parameters(),
			['courseid' => $courseid, 'apikey' => $apikey]);

		$context = context_course::instance($courseid);
		self::validate_context($context);

		$account = $DB->get_record('interactivid_accounts', ['courseid' => $courseid, 'apikey' => $apikey]);
		if (!$account)
		{
			return [
				'table_html' => '',
				'deleted' => false,
				'warnings' => ['API key could not be found.'],
			];			
		}
		else
		{
			// Make sure there's no activities using this API key. If "deletioninprogress" is listed
			// in mdl_coursemodules, that's okay.
			$sql = "SELECT i.*
					FROM mdl_interactivid i
					INNER JOIN mdl_interactivid_videos iv ON i.videoid = iv.id
					WHERE iv.accountid = :accountid
						AND i.id IN (
							SELECT instance
							FROM mdl_course_modules cm
							INNER JOIN mdl_modules m ON cm.module = m.id
							WHERE m.name = 'interactivid'
								AND cm.deletioninprogress = 0
								AND cm.course = i.course)";
			if ($DB->record_exists_sql($sql, ['accountid' => $account->id]))
			{
				error_log('EXISTS' . $account->id);
				return [
					'table_html' => '',
					'deleted' => false,
					'warnings' => ['There are InteractiVid activities created in this course using this API key. You must delete them first.'],
				];			
			}
			else
			{
				interactivid_delete_apikey($courseid, $apikey);

				// Delete the records in interactivid_videos as well.
				$DB->delete_records('interactivid_videos', ['accountid' => $account->id]);

				return [
					'table_html' => interactivid_generate_accounts_html(interactivid_get_accounts($courseid)),
					'deleted' => true,
					'warnings' => [],
				];
			}
		}
	}

    /**
     * @return external_single_structure
     */
    public static function delete_api_key_returns() {
        return new external_single_structure(
            array(
            	'table_html' => new external_value(PARAM_RAW, 'The table HTML.'),
            	'deleted' => new external_value(PARAM_BOOL, 'Whether the API key was deleted.'),
                'warnings'  => new external_warnings(),
            )
        );
    }

    /**
     * @return external_single_structure
     */
    private static function get_account_list_structure($required = true) {
        return new external_single_structure(
            array(
                'id' => new external_value(PARAM_INT, 'id'),
                'email' => new external_value(PARAM_RAW, 'email'),
            ), 'accounts', $required
        );
    }

    /**
     * @return external_function_parameters
     */
    public static function get_account_list_parameters() {
		return new external_function_parameters(
			array(
				'courseid' => new external_value(PARAM_INT, 'Course id.'),
			)
		);
    }

	/**
	 * Returns a list of accounts.
	 *
	 * @param int $courseid
	 * @param int $accountid
	 * @return array
	 */
	public static function get_account_list($courseid)
	{
		global $CFG, $DB, $USER;
		require_once($CFG->dirroot . '/mod/interactivid/lib.php');

		$accounts = interactivid_get_accounts($courseid);
		$list = [];
		foreach ($accounts as $acc)
		{
			$list[] = ['id' => $acc->id, 'email' => $acc->email];
		}
		error_log(print_r($accounts, true));

		return ['accounts' => $list];
	}

    /**
     * @return external_single_structure
     */
    public static function get_account_list_returns() {
        return new external_single_structure(
            array(
            	'accounts' => new external_multiple_structure(self::get_account_list_structure()),
            )
        );
    }

	/**
	 * Refreshes a list of videos for a specific InteractiVid account.
	 *
	 * @param int $accountid
	 * @return
	 */
	public static function refresh_videos_for_account($accountid)
	{
		global $CFG;
		require_once($CFG->dirroot . '/mod/interactivid/lib.php');
		interactivid_refresh_videos($accountid);
	}
}