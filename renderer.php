<?php
/**
 * @package   mod_interactivid
 * @copyright 2017 InteractiVid {@link http://interactivid.com}
 */
class user_table implements renderable
{
}

class mod_interactivid_renderer extends plugin_renderer_base
{
    /**
     * Helper method dealing with the fact we can not just fetch the output of flexible_table
     *
     * @param flexible_table $table The table to render
     * @param int $rowsperpage How many assignments to render in a page
     * @param bool $displaylinks - Whether to render links in the table (e.g. downloads would not enable this)
     * @return string HTML
     */
    protected function flexible_table(flexible_table $table, $rowsperpage = 10, $displaylinks = true)
    {
		global $CFG;
		require_once($CFG->libdir . '/tablelib.php');

		$o = '';
		ob_start();

		$table->out($rowsperpage, $displaylinks);
		$o = ob_get_contents();
		ob_end_clean();

		return $o;
    }

	/**
	 * Create a progress bar.
	 *
	 * @param float $percent
	 * @param string $text
	 * @return string HTML
	 */
	protected function progress_bar($percent, $class = 'default', $text = '')
	{
		$html = '
			<div class="interactivid-progress">
				<div class="interactivid-progressbar">
					<div class="interactivid-progressbar-bars ' . $class . '">
						<div class="interactivid-progressbar-hide" style="width: ' . (100 - $percent) . '%">
							<span class="interactivid-progressbar-percent">' . $text . '</span>
						</div>
					</div>
				</div>
			</div>
		';
		return $html;
	}

	/**
	 * Render a report of the user activity.
	 */
	public function user_table($data)
	{
		global $CFG, $COURSE, $OUTPUT;
		require_once($CFG->libdir . '/tablelib.php');

		$type = $data['type'];
		$users = $data['users'];

		$table = new flexible_table('InteractiVid activity');
		$table->define_baseurl(new moodle_url('/mod/interactivid/index.php'));
	//	$table->set_attribute('id', $this->subtype . 'plugins');
		$table->set_attribute('class', 'admintable generaltable');
		$table->sortable(false, 'lastname');
		$table->pagesize(10);
		$table->collapsible(false);

		switch ($type)
		{
			case 'view':
				$table->define_columns([
					'userpix',
					'fullname',
					'results1',
				]);
				$table->define_headers([
					'User picture',
					'First name / Surname',
					'Percentage of video watched',
				]);
				break;
			case 'score':
				$table->define_columns([
					'userpix',
					'fullname',
					'results1',
					'results2',
				]);
				$table->define_headers([
					'User picture',
					'First name / Surname',
					'Correct answers',
					'Grade',
				]);
				break;
			default:
				$table->define_columns([
					'userpix',
					'fullname',
					'results1',
				]);
				$table->define_headers([
					'User picture',
					'First name / Surname',
					'',
				]);
				break;
		}
		$table->setup();
	
		foreach ($users as $u)
		{
			$seconds_str = '';
			$progress_percentage = 0;
			$progress_class = 'default';
			$progress_text = '';
			
			$points_str = '-';
			$grade_str = '-';

			$user_link = $CFG->wwwroot . '/user/view.php?id=' . $u->id . '&course=' . $COURSE->id;

			if (isset($u->percentage_viewed))
				$percentage_viewed = $u->percentage_viewed;

			if (isset($u->points) && isset($u->total_points))
				$points_str = $u->points . '/' . $u->total_points;

			if (isset($u->grade))
			{
				$grade_obj = reset($u->grade->grades);
				if ($grade_obj)
				{
					$grade_str = $grade_obj->str_long_grade;
				}
			}

			switch ($type)
			{
				case 'view':
					if ($u->percentage_viewed)
						$progress_percentage = $u->percentage_viewed;

					if ($u->percentage_viewed <= 0)
						$progress_class = 'default';
					if ($u->percentage_viewed < 50)
						$progress_class = 'red';
					else if ($u->percentage_viewed < 95)
						$progress_class = 'orange';
					else if ($u->percentage_viewed >= 95)
						$progress_class = 'green';

					$progress_text = $progress_percentage . '%';

					$table->add_data([
						$OUTPUT->user_picture($u, ['size' => 50]),
						'<a href="' . $user_link . '">' . $u->firstname . ' ' . $u->lastname . '</a>',
						$this->progress_bar($progress_percentage, $progress_class, $progress_text),
					]);
					break;
				case 'score':
					$table->add_data([
						$OUTPUT->user_picture($u, ['size' => 50]),
						'<a href="' . $user_link . '">' . $u->firstname . ' ' . $u->lastname . '</a>',
						$points_str,
						$grade_str,
					]);
					break;
				default:
					$table->add_data([
						$OUTPUT->user_picture($u, ['size' => 50]),
						'<a href="' . $user_link . '">' . $u->firstname . ' ' . $u->lastname . '</a>',
						'No activity',
					]);
					break;
			}
		}

		$o = '';
		ob_start();
		$table->finish_output();
		$o = ob_get_contents();
		ob_end_clean();

		return $o;
	}
}