<?php
/**
 * @package   mod_interactivid
 * @copyright 2017 InteractiVid {@link http://interactivid.com}
 */

$string['interactivid:activityname'] = 'Activity name';
$string['interactivid:addinstance'] = 'Add a new interactive video';
$string['interactivid:apikey'] = 'API key';
$string['interactivid:apikey_help'] = 'Copy/paste your API key from InteractiVid to retrieve your videos. Log in to InteractiVid, and click "Settings" to find your key.';
$string['interactivid:chooseaccount'] = 'Select your InteractiVid account';
$string['interactivid:chooseaccount_help'] = 'Enter your API key in the "InteractiVid accounts" section and it will show up in this list.';
$string['interactivid:choosevideo'] = 'Select an interactive video';
$string['interactivid:choosevideo_help'] = 'Select a video associated with your InteractiVid account.';
$string['interactivid:grade'] = 'Maximum grade';
$string['interactivid:grade_help'] = 'The maximum number of points for this activity. This is only used if the grading method is set to "Graded."';
$string['interactivid:grademethod'] = 'Grading method';
$string['interactivid:grademethod_help'] = '<p><b>Not graded</b>:</p><p>No score is given to the student.</p><p><b>Graded</b>:</p><p>If quizzes are present in the video, the score is calculated from the total percentage of correct answers.</p><p>If no quizzes are present in the video, the max score is given when the student views 95% of the video.</p>';
$string['interactivid:managegrades'] = 'Review and release grades';
$string['interactivid:headeraccounts'] = 'InteractiVid accounts';
$string['interactivid:headergrades'] = 'Grades';
$string['interactivid:headeroptions'] = 'Options';
$string['interactivid:newapikey'] = 'Add';
$string['interactivid:refreshvideo'] = 'Refresh the list of videos';
$string['interactivid:reviewgrades'] = 'Review grades';
$string['interactivid:submit'] = 'Submit interactive video';
$string['interactivid:updategrades'] = 'Update grades';
$string['interactivid:view'] = 'View interactive video';
$string['interactivid:viewgrades'] = 'View grades';
$string['modulename'] = 'InteractiVid video';
$string['modulename_help'] = 'Connect your <a href="https://interactivid.com/education" target="_blank">InteractiVid for Education</a> account and easily embed your videos in your Moodle course, or take it a step further and turn them into gradable learning activities. Enhance your existing educational YouTube, Vimeo, and Wistia learning content by adding interactive elements. Create download buttons, quizzes, polls, forms, and more to engage your students in deeper learning. Add in-video assessments to ensure comprehension of the subjects, or simply track video playback and ensure your students are watching videos in their entirety before receiving a passing grade.';
$string['modulenameplural'] = 'InteractiVid videos';
$string['pluginadministration'] = 'InteractiVid administration';
$string['pluginname'] = 'InteractiVid';