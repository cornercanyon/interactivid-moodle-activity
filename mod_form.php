<?php
/**
 * @package   mod_interactivid
 * @copyright 2017 InteractiVid {@link http://interactivid.com}
 */

if (!defined('MOODLE_INTERNAL')) {
	die('Direct access to this script is forbidden.'); 
}

require_once($CFG->dirroot . '/course/moodleform_mod.php');
require_once($CFG->dirroot . '/mod/interactivid/lib.php');
require_once($CFG->dirroot . '/mod/interactivid/locallib.php');

class mod_interactivid_mod_form extends moodleform_mod
{
    function definition()
    {
        global $CFG, $COURSE, $DB, $PAGE, $USER;
    	require_once($CFG->dirroot . '/mod/interactivid/locallib.php');

        $mform =& $this->_form;
/*
		// Retrieve the API keys used by the user or within the course.
		// Sort in descending order, so we can default to the first API key as the last API key used.
		$sql = "SELECT DISTINCT a.id, userid, u.firstname, u.lastname, apikey, a.email, MAX(a.timemodified) AS timemodified
				FROM (
					SELECT ia.id, ia.userid, ia.apikey, ia.email, MAX(i.timemodified) AS timemodified
					FROM mdl_interactivid_accounts ia
					INNER JOIN mdl_interactivid_videos iv ON ia.id = iv.accountid
					INNER JOIN mdl_interactivid i ON iv.id = i.videoid
					WHERE ia.userid = :userid1
					GROUP BY ia.id, ia.apikey, ia.email
					UNION
					SELECT ia.id, ia.userid, ia.apikey, ia.email, MAX(i.timemodified) AS timemodified
					FROM mdl_interactivid_accounts ia
					INNER JOIN mdl_interactivid_videos iv ON ia.id = iv.accountid
					INNER JOIN mdl_interactivid i ON iv.id = i.videoid
					WHERE i.course = :courseid
					GROUP BY ia.id, ia.apikey, ia.email
					UNION
					SELECT ia.id, ia.userid, ia.apikey, ia.email, MAX(ia.timemodified) AS timemodified
					FROM mdl_interactivid_accounts ia
					WHERE ia.userid = :userid2
					GROUP BY ia.id, ia.apikey, ia.email) AS a
				INNER JOIN mdl_user u ON a.userid = u.id
				GROUP BY a.id, userid, firstname, lastname, apikey, a.email
				ORDER BY email";
		$accounts = $DB->get_records_sql($sql, ['userid1' => $USER->id, 'userid2' => $USER->id, 'courseid' => $COURSE->id]);
*/
		// Retrieve the API keys used within the course.
		// Sort in descending order, so we can default to the first API key as the last API key used.
		$accounts = interactivid_get_accounts($COURSE->id);

		$accountoptions = [];
		$latest_timestamp = 0;
		if (!isset($selected_account_id))
		{
			foreach ($accounts as $acc)
			{
//				$accountoptions[$acc->id] = $acc->email . ' (API key: ' . $acc->apikey . ')';
				$accountoptions[$acc->id] = $acc->email;
				if ($acc->timemodified >= $latest_timestamp)
				{
					$latest_timestamp = $acc->timemodified;
					$selected_account_id = $acc->id;
				}
			}
		}

		if (empty($accountoptions))
		{
			$accountoptions[0] = 'Please enter your API key in InteractiVid Accounts';
			$selected_account_id = 0;
		}

//		print_r($accounts);

		// Create the video list.
		interactivid_refresh_videos($selected_account_id, $COURSE->id);
		$videos = interactivid_get_videos($selected_account_id);
/*
		$videooptions = [];
		foreach ($videos as $v)
		{
			$videooptions[$v->id] = $v->title . ' (' . interactivid_convert_seconds($v->duration) . ')';
		}
		if (empty($videos))
		{
			$videooptions[0] = '- None -';
		}
*/
		$sql = "SELECT iv.id, '' AS title
				FROM {interactivid_videos} iv
				WHERE accountid IN (SELECT id FROM {interactivid_accounts} ia WHERE ia.courseid = :courseid)";
		$videooptions = $DB->get_records_sql_menu($sql, ['courseid' => $COURSE->id]);
//-------------------------------------------------------------------------------
        $mform->addElement('header', 'general', get_string('general', 'form'));

        $mform->addElement('text', 'name', 'Activity name', // get_string('activityname', 'interactivid'),
        	array('size' => '64'));
        if (!empty($CFG->formatstringstriptags)) {
            $mform->setType('name', PARAM_TEXT);
        } else {
            $mform->setType('name', PARAM_CLEANHTML);
        }
        $mform->addRule('name', null, 'required', null, 'client');
        $mform->addRule('name', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');

        $this->standard_intro_elements('Description'// get_string('description', 'interactivid')
        );

        $mform->addElement('select', 'account', get_string('interactivid:chooseaccount', 'interactivid'), $accountoptions);
		$mform->setType('account', PARAM_INT);
		$mform->setDefault('account', $selected_account_id);
        $mform->addHelpButton('account', 'interactivid:chooseaccount', 'interactivid');

        $mform->addElement('hidden', 'selectedvideoid', 0);
		$mform->setType('selectedvideoid', PARAM_INT);

		$mform->addElement('select', 'videoid', get_string('interactivid:choosevideo', 'interactivid'),
			$videooptions);
//			[]);
		$mform->setType('videoid', PARAM_INT);
		reset($videooptions);
		$mform->setDefault('videoid', key($videooptions));
		$mform->addHelpButton('videoid', 'interactivid:choosevideo', 'interactivid');
/*
        $this->standard_grading_coursemodule_elements();

        $mform->removeElement('grade');
        if (property_exists($this->current, 'grade')) {
            $currentgrade = $this->current->grade;
        } else {
        	print_r($this->current);
            $currentgrade = $this->current->grade;
//          $currentgrade = 100;
        }
*/
        //-------------------------------------------------------------------------------
		$mform->addElement('header', 'accountshdr', get_string('interactivid:headeraccounts', 'interactivid'));

		// Create a table of the API keys.
		$accounts_table = interactivid_generate_accounts_html($accounts);
		$mform->addElement('html', $accounts_table);

        $mform->addElement('text', 'apikey', get_string('interactivid:apikey', 'interactivid'));
		$mform->setType('apikey', PARAM_TEXT);
        $mform->addHelpButton('apikey', 'interactivid:apikey', 'interactivid');

        $mform->addElement('button', 'newapikey', get_string('interactivid:newapikey', 'interactivid'));

        // Grades.
        $mform->addElement('header', 'general', get_string('interactivid:headergrades', 'interactivid'));

        $mform->addElement('select', 'grademethod', get_string('interactivid:grademethod', 'interactivid'),
			interactivid_get_grading_options());
        $mform->addHelpButton('grademethod', 'interactivid:grademethod', 'interactivid');

        $mform->addElement('text', 'grade', get_string('interactivid:grade', 'interactivid'));
		$mform->setType('grade', PARAM_FLOAT);
        $mform->addHelpButton('grade', 'interactivid:grade', 'interactivid');

        //-------------------------------------------------------------------------------
//		$mform->addElement('header', 'optionhdr', 'Options' // get_string('headeroptions', 'interactivid')
//		);

//-------------------------------------------------------------------------------
        $this->standard_coursemodule_elements();
//-------------------------------------------------------------------------------
        $this->add_action_buttons();

		$PAGE->requires->js_call_amd('mod_interactivid/accounts', 'init');
    }

	function data_preprocessing(&$default_values)
	{
    	global $DB, $PAGE;

		if (isset($default_values['videoid']))
		{
			$video = $DB->get_record('interactivid_videos', ['id' => $default_values['videoid']]);
			if ($video)
			{
				$default_values['account'] = $video->accountid;
				$default_values['selectedvideoid'] = $video->id;
			}
		}

//		print_r($default_values);

		// TODO: Use Fragment AMD to load videos? https://docs.moodle.org/dev/Fragment
//		$videos = interactivid_refresh_videos('nhAYpTJra6L9UPqVRgAuazKoCREG6HzuYaso3cPdjBmdrM9i');
//		print_r($videos);
/*
        global $DB;
        if (!empty($this->_instance) && ($options = $DB->get_records_menu('choice_options',array('choiceid'=>$this->_instance), 'id', 'id,text'))
               && ($options2 = $DB->get_records_menu('choice_options', array('choiceid'=>$this->_instance), 'id', 'id,maxanswers')) ) {
            $choiceids=array_keys($options);
            $options=array_values($options);
            $options2=array_values($options2);

            foreach (array_keys($options) as $key){
                $default_values['option['.$key.']'] = $options[$key];
                $default_values['limit['.$key.']'] = $options2[$key];
                $default_values['optionid['.$key.']'] = $choiceids[$key];
            }

        }
*/
	}

    /**
     * Allows module to modify the data returned by form get_data().
     * This method is also called in the bulk activity completion form.
     *
     * Only available on moodleform_mod.
     *
     * @param stdClass $data the form data to be modified.
     */
    public function data_postprocessing($data) {
        parent::data_postprocessing($data);

        // Set up completion section even if checkbox is not ticked
        if (!empty($data->completionunlocked)) {
            if (empty($data->completionsubmit)) {
                $data->completionsubmit = 0;
            }
        }
    }

    /**
     * Enforce validation rules here
     *
     * @param array $data array of ("fieldname"=>value) of submitted data
     * @param array $files array of uploaded files "element_name"=>tmp_file_path
     * @return array
     **/
    public function validation($data, $files) {
        $errors = parent::validation($data, $files);
/*
        // Check open and close times are consistent.
        if ($data['timeopen'] && $data['timeclose'] &&
                $data['timeclose'] < $data['timeopen']) {
            $errors['timeclose'] = get_string('closebeforeopen', 'choice');
        }
*/
		// Make sure we have a video selected.
		if (!isset($data['videoid']))
		{
			$errors['videoid'] = 'A video was not selected.';
		}
        return $errors;
    }

    function add_completion_rules() {
        $mform =& $this->_form;

        $mform->addElement('checkbox', 'completionsubmit', '', get_string('completionsubmit', 'choice'));
        // Enable this completion rule by default.
        $mform->setDefault('completionsubmit', 1);
        return array('completionsubmit');
    }

    function completion_rule_enabled($data) {
        return !empty($data['completionsubmit']);
    }
}

