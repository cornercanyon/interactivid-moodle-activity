<?php
/**
 * @package   mod_interactivid
 * @copyright 2017 InteractiVid {@link http://interactivid.com}
 */

require('../../config.php');
require_once($CFG->dirroot.'/mod/interactivid/lib.php');
//require_once($CFG->dirroot.'/mod/interactivid/locallib.php');
require_once($CFG->libdir.'/completionlib.php');

$id = optional_param('id', 0, PARAM_INT); // Course Module ID

$cm = get_coursemodule_from_id('interactivid', $id);
$course = $DB->get_record('course', ['id' => $cm->course], '*', MUST_EXIST);
require_course_login($course, true, $cm);

$context = context_module::instance($cm->id);
require_capability('mod/interactivid:view', $context);

$interactivid = $DB->get_record('interactivid', ['id' => $cm->instance]);
$video = $DB->get_record('interactivid_videos', ['id' => $interactivid->videoid]);

$PAGE->set_url('/mod/interactivid/view.php', ['id' => $cm->id]);
$PAGE->set_title('Interactive video: ' . $interactivid->name);
$PAGE->set_heading($interactivid->name);

// Log this record in mdl_interactivid_user_views.
interactivid_log_view($interactivid->id, $USER->id);

$html = '';

if (has_capability('mod/interactivid:reviewgrades', $context))
{
	$html .= '
		<p style="text-align: right;">
			<a href="' . $CFG->wwwroot . '/mod/interactivid/index.php?id=' . $cm->id . '">View user activity</a>
		</p>
	';
}

$embed_url = INTERACTIVID_URL . '/play/' . $video->publicid . '/embed.js';
$custom_obj = new stdClass;
$custom_obj->app = 'moodle';
$custom_obj->domain = interactivid_get_domain();
$custom_obj->id1 = $COURSE->id;
$custom_obj->id2 = $interactivid->id;
$custom_obj->external_user_id = $USER->id;
$custom_obj->first_name = $USER->firstname;
$custom_obj->last_name = $USER->lastname;

$embed_url .= '?custom=' . base64_encode(json_encode($custom_obj));

$html .= '
	<script src="' . $embed_url . '"></script>
';

echo $OUTPUT->header();
echo $OUTPUT->heading(format_string($interactivid->name), 2, null);
echo $html;
echo $OUTPUT->footer();