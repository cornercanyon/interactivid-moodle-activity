<?php
/**
 * @package   mod_interactivid
 * @copyright 2017 InteractiVid {@link http://interactivid.com}
 */

defined('MOODLE_INTERNAL') || die();

$plugin->component = 'mod_interactivid'; // Full name of the plugin (used for diagnostics).
$plugin->version  = 2017070201;    // The current module version (Date: YYYYMMDDXX).
$plugin->requires = 2017050500;    // Requires this Moodle version.