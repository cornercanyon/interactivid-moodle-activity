define(['jquery', 'core/ajax', 'core/str', 'core/log'], function($, ajax, str, log) {
	return {
		init: function() {

 			$('select[name="account"]').change(function() {
 				loadVideos();
			});

			$('button[name="newapikey"]').on('click', function(e) {
				e.preventDefault();
				var button = $(this);
				var apikey = $('input[name="apikey"]').val();
				ajax.call([{
					methodname: 'mod_interactivid_add_api_key',
					args: { courseid: getCourse(), apikey: apikey },
					done: function(data) {
						if (data.table_html != '')
						{
							var table = $('table.interactivid_accounts');
							table.replaceWith(data.table_html);

							// Clear the API key field.
							table.find('input[name="apikey"]').val('');

							// Refresh the accounts list.
							refreshAccounts();
						}
					},
					fail: function (ex) {
						console.log(ex);
						alert('Not a valid API key.');
					},
				}]);
//				alert(newapikey);
			});

			$('body').on('click', 'table.interactivid_accounts button.remove', function(e) {
				e.preventDefault();
				var apikey = $(this).parent().prev().text();
				ajax.call([{
					methodname: 'mod_interactivid_delete_api_key',
					args: { courseid: getCourse(), apikey: apikey },
					done: function(data) {
						if (data.deleted == true && data.table_html != '')
						{
							console.log('replacing table');
							$('table.interactivid_accounts').replaceWith(data.table_html);

							// Refresh the accounts list.
							refreshAccounts();
						}
						console.log('data: %o', data);
					},
					fail: function (ex) {
						console.log(ex);
						alert('Could not remove API key.');
					},
				}]);
//				alert(newapikey);
			});

			var refreshAccounts = function() {
				ajax.call([{
					methodname: 'mod_interactivid_get_account_list',
					args: { courseid: getCourse() },
					done: function(data) {
						if (data.accounts)
						{
							var optionsHtml = '';
							$.each(data.accounts, function(i, item) {
								optionsHtml += '<option value="' + item.id + '">' + item.email + '</option>';
							});
							$('select[name="account"]').html(optionsHtml);

							// Load a new videos list.
							loadVideos();
						}
					},
					fail: function (ex) {
						console.log(ex);
						alert('Could not get accounts.');
					},
				}]);
			};

			var loadVideos = function(selectedVideoId) {
				var accountSelect = $('select[name="account"]'),
					videoSelect = $('select[name="videoid"]'),
					accountId;

				accountid = accountSelect.val();
				videoSelect.html('');

				ajax.call([{
					methodname: 'mod_interactivid_get_videos_for_account',
					args: { courseid: getCourse(), accountid: accountid },
					done: function(data) {
						console.log('data: %o', data);
						if (data.videos)
						{
							var optionsHtml = '';
							$.each(data.videos, function(i, item) {
								optionsHtml += '<option value="' + item.id + '">' + item.title + ' (' + item.durationtime + ')</option>';
							});
							if (optionsHtml == '')
							{
								optionsHtml = '<option value="">- None -</option>';
							}
							videoSelect.html(optionsHtml);

							console.log('SVI: %o', selectedVideoId);
							if (typeof selectedVideoId !== 'undefined')
							{
								if (selectedVideoId != '0')
									videoSelect.val(selectedVideoId);
								else
									videoSelect[0].selectedIndex = 0;
							}
							else
							{
								videoSelect[0].selectedIndex = 0;
							}
						}
					},
					fail: function (ex) {
						console.log(ex);
					},
				}]);
			}

			var getCourse = function() {
				var classes = document.body.classList;
				for (var i = 0; i < classes.length; i++)
				{
					if (classes[i].substring(0, 7) == 'course-')
					{
						var courseid = classes[i].substring(7);
						return courseid;
					}
				}
				return;
			};

			var selectedVideoId = $('input[name="selectedvideoid"]').val();
			loadVideos(selectedVideoId);
		},

    };
});