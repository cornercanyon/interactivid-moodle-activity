<?php
function interactivid_get_grading_options()
{
    return array(
//		INTERACTIVID_NOTGRADED => get_string('gradehighest', 'quiz'),
//		INTERACTIVID_GRADED => get_string('gradeaverage', 'quiz'),
//		INTERACTIVID_COMPLETED => get_string('gradeaverage', 'quiz'),
		INTERACTIVID_NOTGRADED => 'Not graded',
		INTERACTIVID_GRADED => 'Graded',
//		INTERACTIVID_COMPLETED => 'Completed',
    );
}

/**
 * Retrieve an updated list of grades from the InteractiVid API.
 * TODO: Retrieve the users/grades with $DB->get_recordset_sql() to save memory.
 *
 * @param stdClass/int $interactivid
 * @param int $userid
 * @return array grade data formatted for the gradebook API
 */
function interactivid_get_grades_for_gradebook($interactivid, $userid = null)
{
	global $DB;

	if (!is_object($interactivid))
		$interactivid = $DB->get_record('interactivid', $interactivid);

	$current_time = time();

	$sql = "SELECT DISTINCT i.*,
				iv.publicid, iv.accountid, iv.userid, iv.title, iv.thumbnail, iv.hostedby, iv.duration,
				ia.email, ia.brand, ia.apikey
			FROM {interactivid} i
			INNER JOIN {interactivid_videos} iv ON i.videoid = iv.id
			INNER JOIN {interactivid_accounts} ia ON iv.accountid = ia.id
			INNER JOIN {interactivid_user_views} iuv ON i.id = iuv.interactivid
			WHERE i.grademethod = 'graded'
				AND iuv.processed = 0
				AND i.id = :id";
	$activities = $DB->get_records_sql($sql, ['id' => $interactivid->id]);
	foreach ($activities as $a)
	{
		$url = INTERACTIVID_APIURL_V1 . '/analytics/charts/' . $a->publicid . '/0/' . $current_time . '?ifnew=1&api_key=' . $a->apikey . '&app=' . INTERACTIVID_APP . '&domain=' . INTERACTIVID_DOMAIN . '&id1=' . $a->course . '&id2=' . $a->id;
		$json = file_get_contents($url);
		$stats = json_decode($json);

		if (!$stats)
			continue;

		if (!isset($stats))
			continue;

		foreach ($stats->data as $stat)
		{
			if (!$stat->name == 'scores')
				continue;

			if (!isset($stat->chartData->users))
				continue;

			$interactivid_users = $stat->chartData->users;
			if (!$interactivid_users)
				continue;

			$graded_users = [];

			foreach ($interactivid_users as $index => $user)
			{
				if (isset($user->id1))
					$user->courseid = $user->id1;
				else
					continue;

				if (isset($user->id2))
					$user->courseid = $user->id2;
				else
					continue;

				$user->gradingtype = $user->type;

				$graded_users[] = $user;
			}
			return $graded_users;
		}
	}
}

/**
 * Create an HTML table for InteractiVid accounts.
 */
function interactivid_generate_accounts_html($accounts)
{
	$table = new html_table();
	$table->head = ['Email', 'API Key', 'Actions'];
	$table->attributes['class'] = 'generaltable interactivid_accounts';

	foreach ($accounts as $accountid => $acc)
	{
		$table->data[] = [
			$acc->email,
			$acc->apikey,
			'<button class="btn btn-danger remove" data-id="' . $acc->id . '">Remove</button>'
		];
	}

	return html_writer::table($table);
}
