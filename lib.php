<?php
/**
 * When it comes to grading InteractiVid activities, here's the rules. These may be subject to
 * revision later on as we make the activity more flexible and diverse:
 *
 * Not graded: The activity doesn't receive a score.
 * Graded: The score is the first attempt by the student. The activity receives a score according to the following rules:
 * 	(1) If there is at least one quiz, the score the user receives on those count toward a percentage of the "grade" field.
 *	(2) If there are no quizzes, we just make sure the user has viewed at least 95% of the video to earn a score.
 *
 * @package   mod_interactivid
 * @copyright 2017 InteractiVid {@link http://interactivid.com}
 */

defined('MOODLE_INTERNAL') || die();

define('INTERACTIVID_NOTGRADED', 'notgraded');
define('INTERACTIVID_GRADED', 'graded');

define('INTERACTIVID_APP', 'moodle');
define('INTERACTIVID_DOMAIN', parse_url($CFG->wwwroot, PHP_URL_HOST));
define('INTERACTIVID_URL', isset($CFG->interactividurl) ? $CFG->interactividurl : 'https://app.interactivid.com');
define('INTERACTIVID_APIURL', INTERACTIVID_URL . '/api');
define('INTERACTIVID_APIURL_V1', INTERACTIVID_APIURL . '/v1');

/**
 * Return the list if Moodle features this module supports
 *
 * @param string $feature FEATURE_xx constant for requested feature
 * @return mixed True if module supports feature, null if doesn't know
 */
function interactivid_supports($feature) {
    switch($feature) {
        case FEATURE_GROUPS:
            return true;
        case FEATURE_GROUPINGS:
            return true;
        case FEATURE_MOD_INTRO:
            return true;
        case FEATURE_COMPLETION_TRACKS_VIEWS:
            return true;
        case FEATURE_COMPLETION_HAS_RULES:
            return true;
        case FEATURE_GRADE_HAS_GRADE:
            return true;
        case FEATURE_BACKUP_MOODLE2:
            return false;
        case FEATURE_SHOW_DESCRIPTION:
            return true;

        default:
            return null;
    }
}

function interactivid_get_app()
{
	return 'moodle';
}

function interactivid_get_domain()
{
	global $CFG;
	return parse_url($CFG->wwwroot, PHP_URL_HOST);
}

/**
 * Returns the URL for the InteractiVid API.
 * TODO: Create a class to manage this sort of thing.
 *
 * @return string
 */
function interactivid_get_apiurl($version = 1)
{
	return INTERACTIVID_APIURL . '/v' . $version . '/';
}

/**
 * Log a user's view of an InteractiVid video.
 */
function interactivid_log_view($interactivid, $userid)
{
	global $DB;
	$uv = $DB->get_record('interactivid_user_views', ['interactivid' => $interactivid, 'userid' => $userid]);
	if (!$uv)
	{
		$uv = new stdClass;
		$uv->interactivid = $interactivid;
		$uv->userid = $userid;
		$uv->timemodified = time();	

		$DB->insert_record('interactivid_user_views', $uv);
	}
	else
	{
		// Reset the record details and update timemodified.
		$uv->processed = 0;
		$uv->details = '';
		$uv->timemodified = time();

		$DB->update_record('interactivid_user_views', $uv);
	}
}

/**
 * Get user data for API key.
 */
function interactivid_get_user($apikey)
{
	$user_json = file_get_contents(interactivid_get_apiurl() . 'user?api_key=' . $apikey);
	$user = json_decode($user_json);
	if ($user)
		return $user;
	else
		return null;
}

/**
 * Delete API key from course. Also 
 */
function interactivid_delete_apikey($courseid, $apikey, $delete_if_used_in_course = false)
{
	global $DB;

	// Make sure there are no activities using this API key.
	$sql = "SELECT i.*, iv.id AS interactivid_video_id, ia.id AS interactivid_account_id
			FROM mdl_interactivid i
			INNER JOIN mdl_interactivid_videos iv ON i.videoid = iv.id
			INNER JOIN mdl_interactivid_accounts ia ON iv.accountid = ia.id
			WHERE ia.courseid = :courseid AND ia.apikey = :apikey";
	$activities = $DB->get_records_sql($sql, ['courseid' => $courseid, 'apikey' => $apikey]);
	if ($activities && $delete_if_used_in_course == false)
		return false;

	$DB->delete_records('interactivid_accounts', ['courseid' => $courseid, 'apikey' => $apikey]);
	return true;
/*
	$interactivids = [];
	$interactivid_videos = [];
	$interactivid_accounts = [];

	foreach ($activities as $act)
	{
		if (!in_array($interactivid_videos))
			$interactivid_videos[] = $act->interactivid_video_id;

		if (!in_array($interactivid_accounts))
			$interactivid_accounts[] = $act->interactivid_account_id;
	}

	if ($delete_if_user_in_course == false)
	{
	}
*/
}

/**
 * Refresh the list of videos for the user.
 *
 * @param int $accountid
 * @return array The videos for the user
 */
function interactivid_refresh_videos($accountid, $courseid)
{
	global $CFG, $DB;
	$account = $DB->get_record('interactivid_accounts', ['id' => $accountid]);
	if (!$account)
		return;

	$course = $DB->get_record('course', ['id' => $courseid]);
	if (!$course)
		return;

	$url = INTERACTIVID_APIURL_V1 . '/video?api_key=' . $account->apikey;
	$videos_json = file_get_contents($url);
//	error_log(print_r($videos_json, true));
	$new_videos = json_decode($videos_json);
	$new_videos_public_ids = [];
	foreach ($new_videos as $nv)
	{
		$new_videos_public_ids[] = $nv->public_id;
	}
//	echo '<pre>'; print_r($new_videos); echo '</pre>';

	$old_videos = $DB->get_records('interactivid_videos', ['accountid' => $accountid]);
	$old_videos_public_ids = [];
	foreach ($old_videos as $ov)
	{
		$old_videos_public_ids[] = $ov->publicid;
	}
//	echo '<pre>'; print_r($old_videos); echo '</pre>';

	// Compare the public_id of the old videos with the new videos. Add new videos.
	foreach ($new_videos as $nv)
	{
		// If the public_id is already in the list, don't do anything. 
		if (in_array($nv->public_id, $old_videos_public_ids))
			continue;

		// If the public_id is not in the list, add it.
		else
		{
			$newvid = new stdClass;
			$newvid->publicid = $nv->public_id;
			$newvid->accountid = $account->id;
//			$newvid->userid = $account->userid;
			$newvid->userid = 2;
			$newvid->title = $nv->project_name;
			$newvid->thumbnail = $nv->video->thumbnail;
			$newvid->hostedby = $nv->video->hosted_by;
			$newvid->duration = $nv->video->duration;
			$newvid->timemodified = time();
			if (!$DB->insert_record('interactivid_videos', $newvid))
			{
				// TODO: Error-handle.
			}
		}
	}

	// Remove old videos if they don't have any records in interactivid.
	foreach ($old_videos as $ov)
	{
		// If the public_id is not in the new list, check if it's still being used somewhere in the course.
		if (!in_array($ov->publicid, $new_videos_public_ids))
		{
//			echo $ov->publicid . '<br />';
//			print_r($new_videos_public_ids);

			// Look for an interactivid activity with this video in the course.
			if (!$DB->record_exists('interactivid', ['course' => $course->id, 'videoid' => $ov->id]))
			{
				if (!$DB->delete_records('interactivid_videos', ['id' => $ov->id]))
				{
					// TODO: Error-handle.
				}
			}
			else
			{
				// TODO: Notify the user that some videos were deleted from the InteractiVid account,
				// but are still being used in Moodle. We will likely want to keep these records for
				// grading purposes. A timedeleted field may be useful to track these activities, too,
				// instead of actually deleting any mdl_interactivid records from Moodle.
			}
		}
	}

	// Retrieve the new list of videos.
	$videos = $DB->get_records('interactivid_videos', ['accountid' => $account->id]);
	return $videos;
}

/**
 * Retrieve the API keys used within the course. Sort in descending order, so we can default to the
 * first API key as the last API key used.
 */
function interactivid_get_accounts($courseid)
{
	global $DB;

	$sql = "SELECT id, userid, firstname, lastname, apikey, email, timemodified
			FROM (
				SELECT DISTINCT ia.id, ia.userid, u.firstname, u.lastname, apikey, ia.email, COALESCE(ia.timemodified, MAX(i.timemodified)) AS timemodified
				FROM mdl_interactivid_accounts ia
				LEFT JOIN mdl_interactivid_videos iv ON ia.id = iv.accountid
				LEFT JOIN mdl_interactivid i ON iv.id = i.videoid
				INNER JOIN mdl_user u ON ia.userid = u.id
				WHERE ia.courseid = :courseid
				GROUP BY ia.id, ia.userid, firstname, lastname, apikey, ia.email) AS a
			ORDER BY a.timemodified DESC";
	$accounts = $DB->get_records_sql($sql, ['courseid' => $courseid]);

	return $accounts;
}

/**
 * Retrieve the list of videos for the user.
 *
 * @param int $accountid
 * @return array The videos for the user
 */
function interactivid_get_videos($accountid)
{
	global $DB;
	$videos = $DB->get_records('interactivid_videos', ['accountid' => $accountid]);
	foreach ($videos as $index => $v)
	{
		$videos[$index]->durationtime = interactivid_convert_seconds($v->duration);
	}
	return $videos;
}

/**
 * Return time from seconds.
 *
 * @param int $seconds
 * @return string
 */
function interactivid_convert_seconds($seconds)
{
	$seconds = $seconds / 1000;
	$hours = floor($seconds / 3600);
	$mins = floor($seconds / 60 % 60);
	$secs = floor($seconds % 60);
	if ($hours == 0)
		$time_formatted = sprintf('%02d:%02d', $mins, $secs);
	else
		$time_formatted = sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
	return $time_formatted;
}

/**
 * Create the embed code for the project with the extra Moodle data included.
 *
 * @param string $apikey
 * @return array The videos for the user
 */
function interactivid_get_embed($apikey)
{
//	global $CFG, $DB;
}

/**
 * Retrieve the session/viewer results for the activity.
 */
function interactivid_get_data_for_instance($interactivid)
{
	global $DB;
	if (is_integer($interactivid))
	{
		$interactivid = $DB->get_record('interactivid', ['id' => $interactivid]);
	}

	if ($interactivid)
	{
		$url = INTERACTIVID_APIURL_V1 . '/api/';
	}
}

/**
 * Create the embed code for the project with the extra Moodle data included.
 *
 * @param stdClass $data
 * @param mod_interactivid_mod_form $form
 * @return int
 */
function interactivid_add_instance(stdClass $data, mod_interactivid_mod_form $form = null)
{
	global $DB;
	$interactivid = new stdClass;
	$interactivid->course = $data->course;
	$interactivid->name = $data->name;
	$interactivid->intro = $data->intro;
	$interactivid->introformat = $data->introformat;
	$interactivid->videoid = $data->videoid;
	$interactivid->grademethod = $data->grademethod;
	$interactivid->grade = $data->grade;
	$interactivid->timemodified = time();
	$interactivid->id = $DB->insert_record('interactivid', $interactivid);
	if (!$interactivid->id)
	{
		// TODO: Error-handle.
	}
	return $interactivid->id;
}

/**
 * Update the instance for the InteractiVid activity.
 *
 * @param stdClass $data
 * @param mod_interactivid_mod_form $form
 * @return int
 */
function interactivid_update_instance(stdClass $data, mod_interactivid_mod_form $form = null)
{
	global $DB;

	$grading_options = interactivid_get_grading_options();

	$interactivid = new stdClass;
	$interactivid->id = $data->instance;
	$interactivid->course = $data->course;
	$interactivid->name = $data->name;
	$interactivid->intro = $data->intro;
	$interactivid->introformat = $data->introformat;
	$interactivid->videoid = $data->videoid;
	$interactivid->grademethod = $data->grademethod;
	$interactivid->grade = $data->grade;
//	$interactivid->gradepass = $data->gradepass;
	$interactivid->timemodified = time();
	if (!$DB->update_record('interactivid', $interactivid))
	{
		// TODO: Error-handle.
	}

	interactivid_update_grades($interactivid);

	return $interactivid->id;
}

/**
 * Delete the instance for the InteractiVid activity.
 *
 * @param int $id
 * @return boolean
 */
function interactivid_delete_instance($id)
{
	global $DB;
	$interactivid = $DB->get_record('interactivid', ['id' => $id]);
	error_log('DELETING');
	if ($interactivid)
	{
		interactivid_grade_item_delete($interactivid);
		$DB->delete_records('interactivid', ['id' => $interactivid->id]);
	}
	return true;
}

/**
 * Create grade item for the activity.
 *
 * @param stdClass $assign record with extra cmidnumber
 * @param array $grades optional array/object of grade(s); 'reset' means reset grades in gradebook
 * @return int 0 if ok, error code otherwise
 */
function interactivid_grade_item_update($interactivid, $grades = null)
{
	global $CFG, $DB;
	require_once($CFG->libdir.'/gradelib.php');

//	print_r($grades);

	$params = ['itemname' => $interactivid->name];

	if ($interactivid->grademethod == 'graded')
	{
		$params['gradetype'] = GRADE_TYPE_VALUE;
		$params['grademax'] = $interactivid->grade;
		$params['grademin'] = 0;
	}
	else
		$params['gradetype'] = GRADE_TYPE_NONE;

	if ($grades === 'reset')
	{
		$params['reset'] = true;
		$grades = NULL;
	}

	return grade_update('mod/interactivid',
						$interactivid->course,
						'mod',
						'interactivid',
						$interactivid->id,
						0,
						$grades,
						$params);
}

/**
 * Update activity grades.
 *
 * @param stdClass $interactivid InteractiVid object
 * @param int $userid specific user only, 0 means all
 * @param bool $nullifnone - not used
 */
function interactivid_update_grades($interactivid, $userid = 0, $nullifnone = true)
{
	global $CFG, $DB;
	require_once($CFG->dirroot . '/mod/interactivid/locallib.php');
	require_once($CFG->libdir . '/gradelib.php');

	if (is_integer($interactivid))
	{
		$interactivid = $DB->get_record('interactivid', ['id' => $interactivid]);
	}

    if ($interactivid->grade == 0)
	{
		// TODO: Are we running this twice when we save the InteractiVid mod_form?
		interactivid_grade_item_update($interactivid);
	}
	else if ($grades = interactivid_get_grades_for_gradebook($interactivid))
	{
		$grade_updates = [];
		foreach ($grades as $g)
		{
//			print_r($g);

			$grade = new stdClass;

			// If the activity didn't have any quizzes, calculate the grade based on whether the user viewed 95% of the video.
			if ($g->gradingtype == 'view')
			{
				$grade->userid = $g->externalUserId;
				if ($g->percentageViewed >= 95)
					$grade->rawgrade = $interactivid->grade;
				else
					$grade->rawgrade = 0;

//				$grade_updates[$userid] = $grade;
				interactivid_grade_item_update($interactivid, $grade);
			}

			// If the activity had quizzes, calculate the grade based on the percentage and maximum grade points.
			else if ($g->gradingtype == 'score')
			{
				$grade->userid = $g->externalUserId;
				$grade->rawgrade = ($interactivid->grade * ($g->percentage / 100));

//				$grade_updates[$userid] = $grade;
				interactivid_grade_item_update($interactivid, $grade);
			}
		}

	}
	else
    {
		interactivid_grade_item_update($interactivid);
    }
}

/**
 * Delete activity grade item.
 *
 * @param stdClass $interactivid InteractiVid object
 * @return grade_item
 */
function interactivid_grade_item_delete($interactivid)
{
	global $CFG;
	require_once($CFG->libdir.  '/gradelib.php');
	return grade_update('mod/interactivid', $interactivid->course, 'mod', 'interactivid', $interactivid->id, 0, null, ['deleted' => 1]);
}

/**
 * Retrieve the data for an activity.
 *
 * @param stdClass/int $interactivid
 * @return array
 */
function interactivid_get_activity_data($interactivid)
{
	global $CFG, $DB;
	require_once($CFG->libdir . '/gradelib.php');

	if (is_object($interactivid))
		$instanceid = $interactivid->id;
	else
		$instanceid = $interactivid;

	$current_time = time();

	$sql = "SELECT DISTINCT i.*,
				iv.publicid, iv.accountid, iv.userid, iv.title, iv.thumbnail, iv.hostedby, iv.duration,
				ia.email, ia.brand, ia.apikey
			FROM {interactivid} i
			INNER JOIN {interactivid_videos} iv ON i.videoid = iv.id
			INNER JOIN {interactivid_accounts} ia ON iv.accountid = ia.id
			WHERE i.id = :id";
	$instance = $DB->get_record_sql($sql, ['id' => $instanceid]);
	$url = INTERACTIVID_APIURL_V1 . '/analytics/charts/' . $instance->publicid . '/0/' . $current_time . '?ifnew=0&api_key=' . $instance->apikey . '&app=' . INTERACTIVID_APP . '&domain=' . INTERACTIVID_DOMAIN . '&id1=' . $instance->course . '&id2=' . $instance->id;
	$json = file_get_contents($url);
	$stats = json_decode($json);

	if (!$stats)
		return;

	if (!isset($stats->data))
		return;

	$data_type = '';
	$total_points = 0;
	$context = context_course::instance($instance->course);
	$enrolled_users = get_enrolled_users($context, 'mod/interactivid:view', 0, 'u.*', 'lastname, firstname');

	foreach ($stats->data as $stat)
	{
		if (!$stat->name == 'scores')
			continue;

		if (!isset($stat->chartData->users))
			continue;
	
		$interactivid_users = $stat->chartData->users;
		if (!$interactivid_users)
			continue;

		foreach ($interactivid_users as $u)
		{
			// TODO: Error-handle?
			if (!$u->externalUserId)
				continue;

			// Don't use any scores that don't belong to users in the enrolled list.
			if (!isset($enrolled_users[$u->externalUserId]))
				continue;

			// The type should be the same for all these results, so just save it here.
			$data_type = $u->type;

			if ($u->type == 'view')
			{
				$enrolled_users[$u->externalUserId]->seconds_viewed = $u->secondsViewed;
				$enrolled_users[$u->externalUserId]->percentage_viewed = $u->percentageViewed;
			}
			else if ($u->type == 'score')
			{
				$enrolled_users[$u->externalUserId]->total_points = $u->totalPoints;
				$enrolled_users[$u->externalUserId]->points = $u->points;
				$enrolled_users[$u->externalUserId]->questions_answered = $u->questionsAnswered;
				$enrolled_users[$u->externalUserId]->percentage = $u->percentage;

				$grades = grade_get_grades($instance->course, 'mod', 'interactivid', $instance->id, $u->externalUserId);
				$grade = reset($grades)[0]; 
				$enrolled_users[$u->externalUserId]->grade = $grade;
			}

			$res = new stdClass;
			$res->id = $user->id;
			$res->firstname = $user->firstname;
			$res->lastname = $user->lastname;
		}

	}
	return ['type' => $data_type, 'users' => $enrolled_users];
}
