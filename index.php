<?php
/**
 * @package   mod_interactivid
 * @copyright 2017 InteractiVid {@link http://interactivid.com}
 */

require('../../config.php');
require_once($CFG->dirroot . '/mod/interactivid/lib.php');
require_once($CFG->dirroot . '/mod/interactivid/locallib.php');
require_once($CFG->libdir . '/completionlib.php');
require_once($CFG->libdir . '/tablelib.php');

$id = optional_param('id', 0, PARAM_INT); // Course Module ID

$cm = get_coursemodule_from_id('interactivid', $id);
$course = $DB->get_record('course', ['id' => $cm->course], '*', MUST_EXIST);
require_course_login($course, true, $cm);

$context = context_module::instance($cm->id);
require_capability('mod/interactivid:viewgrades', $context);

$interactivid = $DB->get_record('interactivid', ['id' => $cm->instance]);
$video = $DB->get_record('interactivid_videos', ['id' => $interactivid->videoid]);

$PAGE->set_url('/mod/interactivid/index.php', ['id' => $cm->id]);
$PAGE->set_title('Interactive video: ' . $interactivid->name);
$PAGE->set_heading($interactivid->name);

$renderer = $PAGE->get_renderer('mod_interactivid');

// Log this record in mdl_interactivid_user_views.
// interactivid_log_view($interactivid->id, $USER->id);

// Get the data for this activity and course.
$data = interactivid_get_activity_data($interactivid);

$html = '';

$html .= '
	<p style="text-align: right;">
		<a href="' . $CFG->wwwroot . '/mod/interactivid/view.php?id=' . $cm->id . '">View video</a>
	</p>
';

echo $OUTPUT->header();
echo $OUTPUT->heading(format_string($interactivid->name) . ' user activity', 2, null);

if ($data)
{
	echo $renderer->user_table($data);
}

echo $OUTPUT->footer();