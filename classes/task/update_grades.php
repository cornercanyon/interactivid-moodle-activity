<?php
// Use php /var/www/moodle.k12online.org/admin/tool/task/cli/schedule_task.php --execute=\\mod_interactivid\\task\\update_grades to test
namespace mod_interactivid\task;

class update_grades extends \core\task\scheduled_task
{
    public function get_name()
    {
		// Shown in admin screens
		return get_string('interactivid:updategrades', 'interactivid');
    }
                                                                     
    public function execute()
    {
    	global $CFG, $DB;
    	require_once($CFG->dirroot . '/mod/interactivid/lib.php');
    	require_once($CFG->libdir . '/gradelib.php');

		$sql = "SELECT DISTINCT i.*
				FROM {interactivid} i";
//				INNER JOIN {interactivid_user_views} iuv ON i.id = iuv.interactivid
//				WHERE iuv.processed = 0";
		$activities = $DB->get_records_sql($sql);
		foreach ($activities as $a)
		{
			interactivid_update_grades($a);
		}
    }
}
?>